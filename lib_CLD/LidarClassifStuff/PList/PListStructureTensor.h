#ifndef __P_LIST_STRUCTURETENSOR_H__
#define __P_LIST_STRUCTURETENSOR_H__

#include "PList.h"
#include "../Struct/StructureTensor.h"
#include "../AttributeList.h"

class PListStructureTensor : public PList
{
public :
TAttribute< float >gx;
TAttribute< float >gy;
TAttribute< float >gz;
TAttribute< float >lambda1;
TAttribute< float >lambda2;
TAttribute< float >lambda3;
TAttribute< float >sigma1;
TAttribute< float >sigma2;
TAttribute< float >sigma3;
TAttribute< float >eigenVector1x;
TAttribute< float >eigenVector1y;
TAttribute< float >eigenVector1z;
TAttribute< float >eigenVector2x;
TAttribute< float >eigenVector2y;
TAttribute< float >eigenVector2z;
TAttribute< float >eigenVector3x;
TAttribute< float >eigenVector3y;
TAttribute< float >eigenVector3z;

PListStructureTensor() : PList(),
gx("gx",0),
gy("gy",0),
gz("gz",0),
lambda1("lambda1",0),
lambda2("lambda2",0),
lambda3("lambda3",0),
sigma1("sigma1",0),
sigma2("sigma2",0),
sigma3("sigma3",0),
eigenVector1x("eigenVector1x",0),
eigenVector1y("eigenVector1y",0),
eigenVector1z("eigenVector1z",0),
eigenVector2x("eigenVector2x",0),
eigenVector2y("eigenVector2y",0),
eigenVector2z("eigenVector2z",0),
eigenVector3x("eigenVector3x",0),
eigenVector3y("eigenVector3y",0),
eigenVector3z("eigenVector3z",0)
{
    //BV: comented useless attribs
AddAttribute(&gx);
AddAttribute(&gy);
AddAttribute(&gz);
AddAttribute(&lambda1);
AddAttribute(&lambda2);
AddAttribute(&lambda3);
AddAttribute(&sigma1);
AddAttribute(&sigma2);
AddAttribute(&sigma3);
AddAttribute(&eigenVector1x);
AddAttribute(&eigenVector1y);
AddAttribute(&eigenVector1z);
AddAttribute(&eigenVector2x);
AddAttribute(&eigenVector2y);
AddAttribute(&eigenVector2z);
AddAttribute(&eigenVector3x);
AddAttribute(&eigenVector3y);
AddAttribute(&eigenVector3z);
}

StructStructureTensor Get(const Lidar::LidarDataContainer::iterator it)
{
return StructStructureTensor(
gx.Value(it),
gy.Value(it),
gz.Value(it),
lambda1.Value(it),
lambda2.Value(it),
lambda3.Value(it),
sigma1.Value(it),
sigma2.Value(it),
sigma3.Value(it),
eigenVector1x.Value(it),
eigenVector1y.Value(it),
eigenVector1z.Value(it),
eigenVector2x.Value(it),
eigenVector2y.Value(it),
eigenVector2z.Value(it),
eigenVector3x.Value(it),
eigenVector3y.Value(it),
eigenVector3z.Value(it)
);
}

void Get(StructStructureTensor& struct_structureTensor, const Lidar::LidarDataContainer::iterator it)
{
struct_structureTensor.gx = gx.Value(it);
struct_structureTensor.gy = gy.Value(it);
struct_structureTensor.gz = gz.Value(it);
struct_structureTensor.lambda1 = lambda1.Value(it);
struct_structureTensor.lambda2 = lambda2.Value(it);
struct_structureTensor.lambda3 = lambda3.Value(it);
struct_structureTensor.sigma1 = sigma1.Value(it);
struct_structureTensor.sigma2 = sigma2.Value(it);
struct_structureTensor.sigma3 = sigma3.Value(it);
struct_structureTensor.eigenVector1x = eigenVector1x.Value(it);
struct_structureTensor.eigenVector1y = eigenVector1y.Value(it);
struct_structureTensor.eigenVector1z = eigenVector1z.Value(it);
struct_structureTensor.eigenVector2x = eigenVector2x.Value(it);
struct_structureTensor.eigenVector2y = eigenVector2y.Value(it);
struct_structureTensor.eigenVector2z = eigenVector2z.Value(it);
struct_structureTensor.eigenVector3x = eigenVector3x.Value(it);
struct_structureTensor.eigenVector3y = eigenVector3y.Value(it);
struct_structureTensor.eigenVector3z = eigenVector3z.Value(it);
}

void Set(const Lidar::LidarDataContainer::iterator it, const StructStructureTensor& struct_structureTensor)
{
gx.Value(it) = struct_structureTensor.gx;
gy.Value(it) = struct_structureTensor.gy;
gz.Value(it) = struct_structureTensor.gz;
lambda1.Value(it) = struct_structureTensor.lambda1;
lambda2.Value(it) = struct_structureTensor.lambda2;
lambda3.Value(it) = struct_structureTensor.lambda3;
sigma1.Value(it) = struct_structureTensor.sigma1;
sigma2.Value(it) = struct_structureTensor.sigma2;
sigma3.Value(it) = struct_structureTensor.sigma3;
eigenVector1x.Value(it) = struct_structureTensor.eigenVector1x;
eigenVector1y.Value(it) = struct_structureTensor.eigenVector1y;
eigenVector1z.Value(it) = struct_structureTensor.eigenVector1z;
eigenVector2x.Value(it) = struct_structureTensor.eigenVector2x;
eigenVector2y.Value(it) = struct_structureTensor.eigenVector2y;
eigenVector2z.Value(it) = struct_structureTensor.eigenVector2z;
eigenVector3x.Value(it) = struct_structureTensor.eigenVector3x;
eigenVector3y.Value(it) = struct_structureTensor.eigenVector3y;
eigenVector3z.Value(it) = struct_structureTensor.eigenVector3z;
}

void Set(
const Lidar::LidarDataContainer::iterator it,
const float _gx = 0,
const float _gy = 0,
const float _gz = 0,
const float _lambda1 = 0,
const float _lambda2 = 0,
const float _lambda3 = 0,
const float _sigma1 = 0,
const float _sigma2 = 0,
const float _sigma3 = 0,
const float _eigenVector1x = 0,
const float _eigenVector1y = 0,
const float _eigenVector1z = 0,
const float _eigenVector2x = 0,
const float _eigenVector2y = 0,
const float _eigenVector2z = 0,
const float _eigenVector3x = 0,
const float _eigenVector3y = 0,
const float _eigenVector3z = 0
)
{
gx.Value(it) = _gx;
gy.Value(it) = _gy;
gz.Value(it) = _gz;
lambda1.Value(it) = _lambda1;
lambda2.Value(it) = _lambda2;
lambda3.Value(it) = _lambda3;
sigma1.Value(it) = _sigma1;
sigma2.Value(it) = _sigma2;
sigma3.Value(it) = _sigma3;
eigenVector1x.Value(it) = _eigenVector1x;
eigenVector1y.Value(it) = _eigenVector1y;
eigenVector1z.Value(it) = _eigenVector1z;
eigenVector2x.Value(it) = _eigenVector2x;
eigenVector2y.Value(it) = _eigenVector2y;
eigenVector2z.Value(it) = _eigenVector2z;
eigenVector3x.Value(it) = _eigenVector3x;
eigenVector3y.Value(it) = _eigenVector3y;
eigenVector3z.Value(it) = _eigenVector3z;
}

};
#endif //__P_LIST_STRUCTURETENSOR_H__
