#ifndef __P_LIST_COORD_H__
#define __P_LIST_COORD_H__

#include "PList.h"
#include "../Struct/StructCoord.h"

class PListCoord : public PList
{
public :
TAttribute<float> x;
TAttribute<float> y;
TAttribute<float> z;

PListCoord(std::string x_name="x", std::string y_name="y", std::string z_name="z") : PList(),
x(x_name,0),
y(y_name,0),
z(z_name,0)
{
AddAttribute(&x);
AddAttribute(&y);
AddAttribute(&z);
}

StructCoord Get(const Lidar::LidarDataContainer::iterator it)
{
return StructCoord(
x.Value(it),
y.Value(it),
z.Value(it)
);
}

void Get(StructCoord& struct_coord, const Lidar::LidarDataContainer::iterator it)
{
struct_coord.x = x.Value(it);
struct_coord.y = y.Value(it);
struct_coord.z = z.Value(it);
}

void Set(const Lidar::LidarDataContainer::iterator it, StructCoord& struct_coord)
{
x.Value(it) = struct_coord.x;
y.Value(it) = struct_coord.y;
z.Value(it) = struct_coord.z;
}

void Set(
const Lidar::LidarDataContainer::iterator it,
const float _x = 0,
const float _y = 0,
const float _z = 0
)
{
x.Value(it) = _x;
y.Value(it) = _y;
z.Value(it) = _z;
}

};
#endif //__P_LIST_COORD_H__
