#ifndef __P_LIST_DIMENSIONALITY_H__
#define __P_LIST_DIMENSIONALITY_H__

#include "PList.h"
#include "../Struct/StructDimensionality.h"

class PListDimensionality : public PList
{
public :
TAttribute< float >D1;
TAttribute< float >D2;
TAttribute< float >D3;
TAttribute< float >entropy;
TAttribute< int >Dmax;
TAttribute< int >Dmax_of_neighbors;
TAttribute< float >similarity;
TAttribute< int >k;
TAttribute< float >radius;
TAttribute< int >nb_labellings;

PListDimensionality() : PList(),
D1("D1",0),
D2("D2",0),
D3("D3",0),
entropy("entropy",1),
Dmax("Dmax",0),
Dmax_of_neighbors("Dmax_of_neighbors",0),
similarity("similarity",0),
k("k",0),
radius("radius",0),
nb_labellings("nb_labellings",0)
{
    // BV: removed uncomputed attributes
AddAttribute(&D1);
AddAttribute(&D2);
AddAttribute(&D3);
AddAttribute(&entropy);
AddAttribute(&Dmax);
//AddAttribute(&Dmax_of_neighbors);
AddAttribute(&similarity);
AddAttribute(&k);
AddAttribute(&radius);
//AddAttribute(&nb_labellings);
}

StructDimensionality Get(const Lidar::LidarDataContainer::iterator it)
{
return StructDimensionality(
D1.Value(it),
D2.Value(it),
D3.Value(it),
entropy.Value(it),
Dmax.Value(it),
Dmax_of_neighbors.Value(it),
similarity.Value(it),
k.Value(it),
radius.Value(it),
nb_labellings.Value(it)
);
}

void Get(StructDimensionality& struct_dimensionality, const Lidar::LidarDataContainer::iterator it)
{
struct_dimensionality.D1 = D1.Value(it);
struct_dimensionality.D2 = D2.Value(it);
struct_dimensionality.D3 = D3.Value(it);
struct_dimensionality.entropy = entropy.Value(it);
struct_dimensionality.Dmax = Dmax.Value(it);
struct_dimensionality.Dmax_of_neighbors = Dmax_of_neighbors.Value(it);
struct_dimensionality.similarity = similarity.Value(it);
struct_dimensionality.k = k.Value(it);
struct_dimensionality.radius = radius.Value(it);
struct_dimensionality.nb_labellings = nb_labellings.Value(it);
}

void Set(const Lidar::LidarDataContainer::iterator it, const StructDimensionality& struct_dimensionality)
{
D1.Value(it) = struct_dimensionality.D1;
D2.Value(it) = struct_dimensionality.D2;
D3.Value(it) = struct_dimensionality.D3;
entropy.Value(it) = struct_dimensionality.entropy;
Dmax.Value(it) = struct_dimensionality.Dmax;
Dmax_of_neighbors.Value(it) = struct_dimensionality.Dmax_of_neighbors;
similarity.Value(it) = struct_dimensionality.similarity;
k.Value(it) = struct_dimensionality.k;
radius.Value(it) = struct_dimensionality.radius;
nb_labellings.Value(it) = struct_dimensionality.nb_labellings;
}

void Set(
const Lidar::LidarDataContainer::iterator it,
const float _D1 = 0,
const float _D2 = 0,
const float _D3 = 0,
const float _entropy = 1,
const int _Dmax = 0,
const int _Dmax_of_neighbors = 0,
const float _similarity = 0,
const int _k = 0,
const float _radius = 0,
const int _nb_labellings = 0
)
{
D1.Value(it) = _D1;
D2.Value(it) = _D2;
D3.Value(it) = _D3;
entropy.Value(it) = _entropy;
Dmax.Value(it) = _Dmax;
Dmax_of_neighbors.Value(it) = _Dmax_of_neighbors;
similarity.Value(it) = _similarity;
k.Value(it) = _k;
radius.Value(it) = _radius;
nb_labellings.Value(it) = _nb_labellings;
}

};
#endif //__P_LIST_DIMENSIONALITY_H__
