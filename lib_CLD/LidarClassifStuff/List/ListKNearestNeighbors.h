#ifndef __LIST_K_NEAREST_NEIGHBORS_H__
#define __LIST_K_NEAREST_NEIGHBORS_H__

#include <vector>
#include <string.h>
#include <iostream>

#include "LidarFormat/LidarDataContainer.h"

#include "ListSerie.h"


class ListKNearestNeighbors : public ListSerie<int>
{
        typedef ListSerie<int> mother;
public:

#define neighbor_size serie_size
#define neighbor serie
#define neighbor_name "neighbor"

    static int GetSize(Lidar::LidarDataContainer& lidarDataContainer)
    {
        return ListSerie<int>::GetSize(neighbor_name, lidarDataContainer);
    }

    ListKNearestNeighbors(Lidar::LidarDataContainer& lidarDataContainer):mother(neighbor_name, lidarDataContainer)
    {}

    ListKNearestNeighbors(const int k=0):mother(neighbor_name, k)
    {}

    //le numero du dernier voisin = le nombre de voisins (point courant exclu)
    const int kMax(){ return neighbor.size()-1; }

    unsigned int index(Lidar::LidarDataContainer::iterator itPoint, const int i)
    {
        if(i<=0) return neighbor.at(0).Value(itPoint);
        if(neighbor.size()==0) std::cout << "il faut initialiser avec le nombre de voisins indexes" << std::endl;
        if(i>=neighbor.size())
        {
            std::cout << "k trop grand " <<i<< std::endl;
            return neighbor.at(neighbor.size() - 1).Value(itPoint);
        }
        return neighbor.at(i).Value(itPoint);
    }

    /*
    Lidar::LidarDataContainer::iterator itNeighbor(Lidar::LidarDataContainer::iterator& itPoint, const int i)
    {
        return itBegin() + index(itPoint,i);
    }
    */
    
    /*
private:
    static const std::string name(const int i)
    {
        std::ostringstream os;
        os << "neighbor_" << i;
        return os.str();
    }
    */
};

#endif // __LIST_K_NEAREST_NEIGHBORS_H__
