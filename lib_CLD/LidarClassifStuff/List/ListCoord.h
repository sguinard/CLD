#ifndef __LIST_COORD_H__
#define __LIST_COORD_H__

#include "../PList/PListCoord.h"
#include "LiteGeom/LgPoint3.hpp"
#include "LiteGeom/LgPoint2.hpp"

class ListCoord : public PListCoord
{
public :

ListCoord(std::string x_name="x", std::string y_name="y", std::string z_name="z"): PListCoord(x_name, y_name, z_name){}

float Distance2(Lidar::LidarDataContainer::iterator it1, Lidar::LidarDataContainer::iterator it2) const
{
        float dx = x.Value(it1) - x.Value(it2);
        float dy = y.Value(it1) - y.Value(it2);
        float dz = z.Value(it1) - z.Value(it2);
        return dx*dx + dy*dy + dz*dz;
}

float Distance(Lidar::LidarDataContainer::iterator it1, Lidar::LidarDataContainer::iterator it2) const
{
        return std::sqrt( Distance2(it1,it2) );
}

Lg::Point3f Point(const Lidar::LidarEcho & echo)const
{
    return Lg::Point3f(x.Value(echo), y.Value(echo), z.Value(echo));
}

Lg::Point3f Point(const Lidar::LidarDataContainer::iterator it)const
{
   return Lg::Point3f(x.Value(it), y.Value(it), z.Value(it));
}

Lg::Point2f XY(const Lidar::LidarDataContainer::iterator it)const
{
    return Lg::Point2f(x.Value(it), y.Value(it));
}

};
#endif //__LIST_COORD_H__
