#ifndef __LIST_DIMENSIONALITY_H__
#define __LIST_DIMENSIONALITY_H__

#include "../PList/PListDimensionality.h"

#include "../PCA/Dimensionality.h"

class ListDimensionality : public PListDimensionality
{
public :

ListDimensionality() : PListDimensionality(){}

void Set(Lidar::LidarDataContainer::iterator& itPoint, const Dimensionality<float>& dim)
{
    D1.Value(itPoint) = dim.D1();
    D2.Value(itPoint) = dim.D2();
    D3.Value(itPoint) = dim.D3();
    entropy.Value(itPoint) = dim.Entropy();
    Dmax.Value(itPoint) = dim.DMax();
}

void Set(Lidar::LidarDataContainer::iterator& itPoint, const StructDimensionality& dimensionality)
{
    D1.Value(itPoint) = dimensionality.D1;
    D2.Value(itPoint) = dimensionality.D2;
    D3.Value(itPoint) = dimensionality.D3;
    entropy.Value(itPoint) = dimensionality.entropy;
    Dmax.Value(itPoint) = dimensionality.Dmax;
}

};
#endif //__LIST_DIMENSIONALITY_H__
