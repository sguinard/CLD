#ifndef __ALGO_MIN_MAX_H__
#define __ALGO_MIN_MAX_H__

namespace Algo{

struct CompareLeq
{
    template<typename t_key>
    bool operator()(const t_key& key, const t_key& best_key)
    {
        return key <= best_key;
    }
};

struct CompareGeq
{
    template<typename t_key>
    bool operator()(const t_key& key, const t_key& best_key)
    {
        return key >= best_key;
    }
};

template<typename t_key, typename t_id, class t_compare>
class Best
{
    t_key m_best_key;
    t_id  m_best_id;
    t_compare m_compare;

    int m_nb_obs;

public :

    Best():m_nb_obs(0){}

    Best(const t_key& default_key, const t_id& default_id)
    {
        Init(default_key, default_id);
    }

    void Init(const t_key& default_key, const t_id& default_id)
    {
        m_best_key = default_key;
        m_best_id  = default_id;
        m_nb_obs  = 0;
    }

    /// @brief les valeurs par defaut ne sont prises en compte que si cette fonction n'est jamais appelee
    void operator()(const t_key& key, const t_id& id)
    {
        if((m_nb_obs == 0) || m_compare(key,m_best_key))
        {
            m_best_key = key;
            m_best_id  = id;
        }
        ++m_nb_obs;
    }

    t_key Key()const{return m_best_key;}
    t_id   Id()const{return m_best_id;}
    int NbObs()const{return m_nb_obs;}
};

template<typename t_key, typename t_id>
class Min : public Best< t_key, t_id, CompareLeq>
{
    typedef Best< t_key, t_id, CompareLeq> mother;
public :
    Min():mother(){}
    Min(const t_key& default_key, const t_id& default_id):mother(default_key, default_id){}
};

template<typename t_key, typename t_id>
class Max : public Best< t_key, t_id, CompareGeq>
{
    typedef Best< t_key, t_id, CompareGeq> mother;
public :
    Max():mother(){}
    Max(const t_key& default_key, const t_id& default_id):mother(default_key, default_id){}
};

template<typename t_key, typename t_id>
struct MinMax
{
    Min<t_key, t_id> min;
    Max<t_key, t_id> max;
public :

    void Init(const t_key& default_key, const t_id& default_id)
    {
        min.Init(default_key, default_id);
        max.Init(default_key, default_id);
    }

    MinMax(){}

    MinMax(const t_key& default_key, const t_id& default_id):
        min(default_key, default_id),
        max(default_key, default_id)
    {}

    void operator()(const t_key& key, const t_id& id)
    {
        min(key, id);
        max(key, id);
    }
};


template<typename t_key, typename t_iterator, class t_access_value>
void FindMinMax(const t_iterator first, const int size, t_access_value& access_value, t_key& min, t_key& max)
{
    //Initialize with first value.
    MinMax<t_key, int> min_max(access_value(first), 0);

    //Run container.
    t_iterator it = first;
    for(int i=0; i!=size; ++i, ++it) min_max(access_value(it), i);

    //Fill results.
    min = min_max.min.Key();
    max = min_max.max.Key();
}


//Best< , , CompareGeq> Max;

}

#endif //__ALGO_MIN_MAX_H__
