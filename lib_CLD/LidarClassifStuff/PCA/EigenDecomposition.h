#ifndef LAMBDAS_HPP_
#define LAMBDAS_HPP_

#include <vector>
#include <iostream>
#include <algorithm>
#include <math.h>
#include <iomanip>

#include "PrincipalComponentAnalysis.h"
//#include "EigenMatrix.h"

// import most common Eigen types
using namespace Eigen;


template<typename _MatrixType, typename _VectorType>
class EigenDecomposition
{
    public :
            EigenDecomposition(const unsigned int dimension):
             m_d(dimension)
            {}

            template<typename T>
            EigenDecomposition(PrincipalComponentAnalysis<T,T,3>& pca)
            {
                if(pca.isVoid())
                {
                    std::cout << "PCA is void." << std::endl;
                }
                else
                {
                    //std::cout << " templateFindCovarianceMatrix ";
                    Stats::FindCovarianceMatrix<3,M2<T, T, 3>,_MatrixType>(pca.moments(), m_covarianceMatrix);
                    //std::cout << " templateFindEigen ";
                    Stats::Solve<_MatrixType,_VectorType>(m_covarianceMatrix,m_eigenvectors,m_eigenvalues);
                    //std::cout << " getE ";
                    pca.getE(m_E);
                    //std::cout << " SetSigmas ";
                    //SetSigmas();
                }
            }

            EigenDecomposition(
                    const unsigned int d,
                    _MatrixType& covarianceMatrix,
                    _MatrixType& eigenvectors,
                    _VectorType& eigenvalues,
                    _VectorType& E):
                    m_d(d),
                    m_covarianceMatrix(covarianceMatrix),
                    m_eigenvectors(eigenvectors),
                    m_eigenvalues(eigenvalues),
                    m_E(E)
            {
                //SetSigmas();
            }

            ~EigenDecomposition()
            {}

            float distanceE()const{return std::sqrt(m_covarianceMatrix(0,0)+m_covarianceMatrix(1,1)+m_covarianceMatrix(2,2));}

            float distanceE(_VectorType& point)const{return distance(point, m_E);}

            float newDistanceE(_VectorType& point)const
            {
                _VectorType p = point - m_E;
                p = m_covarianceMatrix * p;
                return distance2(p);
            }

            static float distance2(_VectorType& a){return a.squaredNorm();}

            static float distance2(_VectorType& a, _VectorType& b)
            {
                _VectorType diff = a - b;
                return distance2(diff);
            }

            static float distance(_VectorType& a){return std::sqrt(distance2(a));}
            static float distance(_VectorType& a, _VectorType& b){return std::sqrt(distance2(a,b));}

            void get(_VectorType& lambdas)const{lambdas = m_eigenvalues;}


            const _VectorType & E()const{return m_E;}

            const unsigned int d()const{return m_d;}


            float E3X()const{return e3x<_MatrixType >(m_eigenvectors);}
            float E2X()const{return e2x<_MatrixType >(m_eigenvectors);}
            float E1X()const{return e1x<_MatrixType >(m_eigenvectors);}

            float E3Y()const{return e3y<_MatrixType >(m_eigenvectors);}
            float E2Y()const{return e2y<_MatrixType >(m_eigenvectors);}
            float E1Y()const{return e1y<_MatrixType >(m_eigenvectors);}

            float E3Z()const{return e3z<_MatrixType >(m_eigenvectors);}
            float E2Z()const{return e2z<_MatrixType >(m_eigenvectors);}
            float E1Z()const{return e1z<_MatrixType >(m_eigenvectors);}

            template<typename t_point> t_point E1()const{return t_point(E1X(), E1Y(), E1Z());}
            template<typename t_point> t_point E2()const{return t_point(E2X(), E2Y(), E2Z());}
            template<typename t_point> t_point E3()const{return t_point(E3X(), E3Y(), E3Z());}

            float gx(){ return m_E(0);}
            float gy(){ return m_E(1);}
            float gz(){ return m_E(2);}

            template<typename t_point>
            t_point G()const
            {
                return t_point(m_E(0),m_E(1),m_E(2));
            }

            /*

            template< typename T >
            void eigenMatrix( EigenMatrix< T >& _eigenMatrix )const
            {
                _eigenMatrix.eigenValue1 = Lambda1();
                _eigenMatrix.eigenValue2 = Lambda2();
                _eigenMatrix.eigenValue3 = Lambda3();
                _eigenMatrix.eigenVector3x = e3x();
                _eigenMatrix.eigenVector3y = e3y();
                _eigenMatrix.eigenVector3z = e3z();
                _eigenMatrix.eigenVector2x = e2x();
                _eigenMatrix.eigenVector2y = e2y();
                _eigenMatrix.eigenVector2z = e2z();
                _eigenMatrix.eigenVector1x = e1x();
                _eigenMatrix.eigenVector1y = e1y();
                _eigenMatrix.eigenVector1z = e1z();
            }
            */


            _MatrixType covarianceMatrix(_MatrixType& matrix){matrix = m_covarianceMatrix;}
            _MatrixType eigenvectors(_MatrixType& matrix){matrix = m_eigenvectors;}
            _VectorType eigenvalues(_VectorType& vector){vector = m_eigenvalues;}


            /*
            static float bound(const float value, const float min=0, const float max=1)
            {
                if(value>max) return max;
                if(value<min) return min;
                return value;
            }
            */

            inline float Lambda1()const{return lambda1<_VectorType >(m_eigenvalues);}
            inline float Lambda2()const{return lambda2<_VectorType >(m_eigenvalues);}
            inline float Lambda3()const{return lambda3<_VectorType >(m_eigenvalues);}

private :
            /*
            void SetSigmas()
            {
                float lambda1 = m_eigenvalues(2);
                float lambda2 = m_eigenvalues(1);
                float lambda3 = m_eigenvalues(0);

                lambda3 = std::fabs(lambda3);
                lambda2 = std::max (lambda3, lambda2);
                lambda1 = std::max (lambda2, lambda1);

                m_sigma1 = sqrt(lambda1);
                m_sigma2 = sqrt(lambda2);
                m_sigma3 = sqrt(lambda3);
            }
            */

            unsigned int m_d;
            _MatrixType m_covarianceMatrix;
            _MatrixType m_eigenvectors;
            _VectorType m_eigenvalues;
            _VectorType m_E;

            /*
            float m_sigma1;
            float m_sigma2;
            float m_sigma3;
            */
};

#endif /* LAMBDAS_HPP_ */
