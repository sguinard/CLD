/// @file   Tensor3D.h
/// @author Jerome Demantke
/// @date   2012/11/28

#ifndef __TENSOR_3D_H__
#define __TENSOR_3D_H__

#include <math.h>
//#include "LiteGeom/LgPoint3.hpp"

//#include "LfTools/Geom/BOG.h"

//namespace {

/// @brief Au lieu de stocker les 3 vecteurs de la base orthonormale (e1, e2, e3)
///        il suffit de stocker e1x, e1z, e3x, e3z et les signes.
///        De meme, inutile de stocker sigma2, sigma3 et d3.
template<typename T>
class Tensor3D
{
    Lg::TPoint3<T> m_g;

    T m_e1x, m_e1z;
    T m_e3x, m_e3z;

    bool m_signs[9];

    T m_d1, m_d2;
    T m_sigma1;

    T m_k;

#define BIT_SIGN_E1X 0
#define BIT_SIGN_E1Y 1
#define BIT_SIGN_E1Z 2

#define BIT_SIGN_E2X 3
#define BIT_SIGN_E2Y 4
#define BIT_SIGN_E2Z 5

#define BIT_SIGN_E3X 6
#define BIT_SIGN_E3Y 7
#define BIT_SIGN_E3Z 8

#define POW_E1X 1
#define POW_E1Y 2
#define POW_E1Z 4

#define POW_E2X 8
#define POW_E2Y 16
#define POW_E2Z 32

#define POW_E3X 64
#define POW_E3Y 128
#define POW_E3Z 256

public :

    Tensor3D(){}

    Tensor3D(T gx, T gy, T gz,
             const T abs_e1x, const T abs_e1z,
             const T abs_e3x, const T abs_e3z,
             const int code_signs,
             const T sigma1,
             const T d1,
             const T d2,
             const T k):
        m_g(gx, gy, gz),
        m_e1x(abs_e1x),
        m_e1z(abs_e1z),
        m_e3x(abs_e3x),
        m_e3z(abs_e3z),
        m_sigma1(sigma1),
        m_d1(d1),
        m_d2(d2),
        m_k(k)
    {
        Int2Signs(code_signs);
    }

    Tensor3D(
            const Lg::TPoint3<T>& g,
            const Lg::TPoint3<T>& e1,
            const Lg::TPoint3<T>& e2,
            const Lg::TPoint3<T>& e3,
            const T sigma1,
            const T d1,
            const T d2,
            const T k):
        m_g(g),
        m_sigma1(sigma1),
        m_d1(d1),
        m_d2(d2),
        m_k(k)
    {
        SetE1(e1);
        SetE2(e2);
        SetE3(e3);
    }

    static T AbsY(T x, T z){return sqrt(fabs(1. - x * x - z * z));}

    static Lg::TPoint3<T> E2(const Lg::TPoint3<T>& e1, const Lg::TPoint3<T>& e3){return e1 ^ e3;}

    static T Square(T value){return value * value;}

    T K()const{return m_k;}

    T E1X()const{return SignE1X() * m_e1x;}
    T E3X()const{return SignE3X() * m_e3x;}

    T E1Y()const{return SignE1Y() * AbsY(m_e1x, m_e1z);}
    T E3Y()const{return SignE3Y() * AbsY(m_e3x, m_e3z);}

    T E1Z()const{return SignE1Z() * m_e1z;}
    T E3Z()const{return SignE3Z() * m_e3z;}

    T AbsE1X()const{return m_e1x;}
    T AbsE3X()const{return m_e3x;}

    T AbsE1Y()const{return AbsY(m_e1x, m_e1z);}
    T AbsE3Y()const{return AbsY(m_e3x, m_e3z);}

    T AbsE1Z()const{return m_e1z;}
    T AbsE3Z()const{return m_e3z;}

    Lg::TPoint3<T> E1()const{return Lg::TPoint3<T>(E1X(), E1Y(), E1Z());}
    Lg::TPoint3<T> E3()const{return Lg::TPoint3<T>(E3X(), E3Y(), E3Z());}

    Lg::TPoint3<T> E2()const
    {
        Lg::TPoint3<T> e2 = E2(E1(), E3());

        e2.X() = SignE2X() * fabs(e2.X());
        e2.Y() = SignE2Y() * fabs(e2.Y());
        e2.Z() = SignE2Z() * fabs(e2.Z());

        return e2;
    }

    T Sigma1()const{return m_sigma1;}
    T Sigma2()const{return m_sigma1 * (1 - m_d1);}
    T Sigma3()const{return m_sigma1 * (1 - m_d1 - m_d2);}

    T Lambda1()const{return m_sigma1 * m_sigma1;}
    T Lambda2()const{return Square(Sigma2());}
    T Lambda3()const{return Square(Sigma3());}

    T D1()const{return m_d1;}
    T D2()const{return m_d2;}
    T D3()const{return 1 - m_d1 - m_d2;}

    Lg::TPoint3<T> G()const{return m_g;}

    static T Sign(bool b){if(b) return -1; else return 1;}

    T SignE1X()const{return Sign(m_signs[BIT_SIGN_E1X]);}
    T SignE1Y()const{return Sign(m_signs[BIT_SIGN_E1Y]);}
    T SignE1Z()const{return Sign(m_signs[BIT_SIGN_E1Z]);}

    T SignE2X()const{return Sign(m_signs[BIT_SIGN_E2X]);}
    T SignE2Y()const{return Sign(m_signs[BIT_SIGN_E2Y]);}
    T SignE2Z()const{return Sign(m_signs[BIT_SIGN_E2Z]);}

    T SignE3X()const{return Sign(m_signs[BIT_SIGN_E3X]);}
    T SignE3Y()const{return Sign(m_signs[BIT_SIGN_E3Y]);}
    T SignE3Z()const{return Sign(m_signs[BIT_SIGN_E3Z]);}

    void SetSign(int bit_sign, T value)
    {
        m_signs[bit_sign] = (value < 0);
    }

    inline void SetSignE1X(T value){SetSign(BIT_SIGN_E1X, value);}
    inline void SetSignE1Y(T value){SetSign(BIT_SIGN_E1Y, value);}
    inline void SetSignE1Z(T value){SetSign(BIT_SIGN_E1Z, value);}

    inline void SetSignE2X(T value){SetSign(BIT_SIGN_E2X, value);}
    inline void SetSignE2Y(T value){SetSign(BIT_SIGN_E2Y, value);}
    inline void SetSignE2Z(T value){SetSign(BIT_SIGN_E2Z, value);}

    inline void SetSignE3X(T value){SetSign(BIT_SIGN_E3X, value);}
    inline void SetSignE3Y(T value){SetSign(BIT_SIGN_E3Y, value);}
    inline void SetSignE3Z(T value){SetSign(BIT_SIGN_E3Z, value);}

    void SetSignE1(const Lg::TPoint3<T>& e1)
    {
        SetSignE1X(e1.X());
        SetSignE1Y(e1.Y());
        SetSignE1Z(e1.Z());
    }

    void SetSignE2(const Lg::TPoint3<T>& e2)
    {
        SetSignE2X(e2.X());
        SetSignE2Y(e2.Y());
        SetSignE2Z(e2.Z());
    }

    void SetSignE3(const Lg::TPoint3<T>& e3)
    {
        SetSignE3X(e3.X());
        SetSignE3Y(e3.Y());
        SetSignE3Z(e3.Z());
    }

    void SetE1(const Lg::TPoint3<T>& e1)
    {
        SetSignE1(e1);

        m_e1x = fabs(e1.X());
        m_e1z = fabs(e1.Z());
    }

    void SetE2(const Lg::TPoint3<T>& e2)
    {
        SetSignE2(e2);
    }

    void SetE3(const Lg::TPoint3<T>& e3)
    {
        SetSignE3(e3);

        m_e3x = fabs(e3.X());
        m_e3z = fabs(e3.Z());
    }

    int Signs2Int()const
    {
        int code_signs=0;
        int pow=1;
        for(int i=0; i!=9; ++i, pow*=2)
        {
            if(m_signs[i]) code_signs += pow;
        }
        return code_signs;
    }

    void Int2Signs(int code_signs)
    {
        for(int i=0; i!=9; ++i, code_signs /= 2)
        {
            m_signs[i] = (code_signs % 2 == 1);
        }
    }
};

//}//namespace

#endif //__TENSOR_3D_H__
