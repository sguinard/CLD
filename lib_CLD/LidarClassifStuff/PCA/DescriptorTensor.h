#ifndef DESCRIPTORTENSOR_H
#define DESCRIPTORTENSOR_H

//#include "LiteGeom/LgLine3.hpp"

//#include "LfTools/Geom/IntersectionPlane3D.h"

#include "PrincipalComponentAnalysis.h"
#include "EigenDecomposition.h"
//#include "LfTools/Stats/PCA/Dimensionality.h"
#include "Descriptors.h"
//#include "LfTools/Dimensionality/SpatialAttributes.h"
#include "../Struct/StructureTensor.h"

#include "Sigmas.h"
#include "Tensor3D.h"

class StructureTensorDescriptor : public StructureTensor
{

public:
    StructureTensorDescriptor():
        m_eigen(3)
    {}

    const DimDescriptors<float>& GetDim()const
    {
        return m_dim;
    }

    void operator()(const float x, const float y, const float z, const float weight=1)
    {
        if(m_pcaXYZ.nbObs() == 0) m_first.SetXYZ(x,y,z);

        m_last.SetXYZ(x,y,z);

        m_pcaXYZ.push(x,y,z,weight);
    }

    void Clear(){m_pcaXYZ = PrincipalComponentAnalysis<float,float,3>();}

    template<typename U>
    void Get(Tensor3D<U>& tensor3d)
    {
        tensor3d = Tensor3D<U>(m_eigen.G <Lg::TPoint3<U> >(),
                               m_eigen.E1<Lg::TPoint3<U> >(),
                               m_eigen.E2<Lg::TPoint3<U> >(),
                               m_eigen.E3<Lg::TPoint3<U> >(),
                               m_sigmas.Sigma1(),
                               m_dim.D1(),
                               m_dim.D2(),
                               m_pcaXYZ.nbObs()
                               );
    }

    void FindEigen()
    {
        if(m_pcaXYZ.isVoid())
        {
            std::cout << "PCA is void." << std::endl;
            return;
        }
        else
        {
            m_eigen = EigenDecomposition<Matrix3f,Vector3f>(m_pcaXYZ);
            m_sigmas.SetFromLambdas(m_eigen.Lambda1(), m_eigen.Lambda2(), m_eigen.Lambda3());
            m_dim.Set(m_sigmas);
        }
    }

    StructStructureTensor GetStruct()
    {
        StructStructureTensor structure;

        structure.gx = gx();
        structure.gy = gy();
        structure.gz = gz();

        structure.lambda1 = m_eigen.Lambda1();
        structure.lambda2 = m_eigen.Lambda2();
        structure.lambda3 = m_eigen.Lambda3();

        structure.sigma1 = m_sigmas.Sigma1();
        structure.sigma2 = m_sigmas.Sigma2();
        structure.sigma3 = m_sigmas.Sigma3();

        structure.eigenVector1x = m_eigen.E1X();
        structure.eigenVector1y = m_eigen.E1Y();
        structure.eigenVector1z = m_eigen.E1Z();
        structure.eigenVector2x = m_eigen.E2X();
        structure.eigenVector2y = m_eigen.E2Y();
        structure.eigenVector2z = m_eigen.E2Z();
        structure.eigenVector3x = m_eigen.E3X();
        structure.eigenVector3y = m_eigen.E3Y();
        structure.eigenVector3z = m_eigen.E3Z();

        return structure;
    }

private:
    PrincipalComponentAnalysis<float,float,3> m_pcaXYZ;
    EigenDecomposition<Matrix3f,Vector3f> m_eigen;
    Sigmas<float> m_sigmas;
    DimDescriptors<float> m_dim;
    Lg::Point3f m_first, m_last;

};


#endif // DESCRIPTORTENSOR_H
