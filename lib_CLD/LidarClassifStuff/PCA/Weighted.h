#ifndef __WEIGHTED__H__
#define __WEIGHTED_H__

#include "Vect.h"
#include <algorithm>
#include <limits>

template< typename t_value, typename t_weight>
class WeightedPoint
{
        t_value m_value;
        t_weight m_weight;

        bool m_divided;

        void Divide()
        {
            if(!m_divided)
            {
                if(m_weight != 0) m_value /= m_weight;

                m_divided = true;
            }
        }

        bool IsTooBig()
        {
            return m_weight > 0.1;
        }

public :

        t_value Value()const
        {
           if(m_weight == 0) return m_value * 0;

            if(m_divided) return m_value;

            else          return m_value / m_weight;
        }

        t_value& Value()
        {
            Divide();

            return m_value;
        }

       t_weight Weight()const{return m_weight;}

       t_weight& Weight(){return m_weight;}

        WeightedPoint():m_weight(0),m_divided(false){}

        WeightedPoint(const t_value& _value, const t_weight& _weight=1):m_value(_value),m_weight(_weight),m_divided(false){}

        WeightedPoint(const WeightedPoint& wp):m_value(wp.m_value),m_weight(wp.m_weight),m_divided(wp.m_divided){}

        WeightedPoint(const WeightedPoint& a, const WeightedPoint& b):m_value(a.m_value),m_weight(a.m_weight),m_divided(a.m_divided)
    {
                Accumulate(b);
    }

        void Accumulate(const t_value& _value, const t_weight& _weight=1, const bool _divided = false)
        {
            if(m_divided) m_value *= m_weight;

            if(_divided) m_value += _value * _weight;

            else         m_value += _value;

            m_weight += _weight;

            //if((m_weight!=0) && IsTooBig()) Divide();
        }

        void Accumulate(const WeightedPoint& wp){Accumulate(wp.m_value, wp.m_weight, wp.m_divided);}

        void Copy(const WeightedPoint& wp)
    {
                m_value = wp.m_value;
                m_weight= wp.m_weight;
                m_divided = wp.m_divided;
    }

        void operator = (const WeightedPoint& wp){Copy(wp);}

        void operator += (const WeightedPoint& wp){Accumulate(wp);}

        void operator -= (const WeightedPoint& wp){Accumulate(-wp);}

    WeightedPoint operator + (const WeightedPoint& wp)const
    {
            WeightedPoint out(wp);
            out.Accumulate(m_value, m_weight, m_divided);
            return out;
    }

    WeightedPoint operator - (const WeightedPoint& wp)const
    {
            WeightedPoint out(-wp);
            out.Accumulate(m_value, m_weight, m_divided);
            return out;
    }

    //friend WeightedPoint operator-(const WeightedPoint &wp);
};

// note: this function is not a member function!
template< typename t_value, typename t_weight>
WeightedPoint< t_value, t_weight> operator-(const WeightedPoint< t_value, t_weight> &wp)
{
  return WeightedPoint<t_value, t_weight>(wp.m_value, -wp.m_weight, wp.m_divided);
}


template < typename t_value, typename t_weight, int size>
        class WeightedVector : public Algo::Vect< t_value, size >
{
    t_weight m_weight;

public :

        WeightedVector(const t_value& value= 0, const t_weight weight=0):Algo::Vect< t_value, size >(value), m_weight(weight){}

    t_weight Weight()const{return m_weight;}
    t_weight& Weight(){return m_weight;}
};

#endif //__WEIGHTED_H__
