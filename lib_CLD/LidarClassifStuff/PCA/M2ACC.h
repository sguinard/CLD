#ifndef __M2_ACC_H__
#define __M2_ACC_H__

#include "M2.h"
#include "Vect.h"
#include "CovarianceStorage.h"

#include "SymmetricMatrix.h"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/variates/covariate.hpp>
#include <boost/accumulators/statistics/weighted_mean.hpp>
#include <boost/accumulators/statistics/weighted_covariance.hpp>


template< typename t_value, typename t_weight, int dim>
class M2Acc
{
    typedef
    boost::accumulators::accumulator_set<
        t_value,
            boost::accumulators::stats
            <
                boost::accumulators::tag::weighted_sum,
                boost::accumulators::tag::weighted_mean,
                boost::accumulators::tag::weighted_covariance<t_value, boost::accumulators::tag::covariate1>
            >,
            t_weight
    > t_accumulator_set;

    Storage::SymmetricMatrix< t_accumulator_set> m_cov_storage;

    //Algo::Vect< t_value, dim> mv_mean;
    //CovarianceStorage< t_value, dim > mv_cov;

public :


        M2Acc():m_cov_storage(dim){}

        M2Acc(const M2Acc& m2):
                m_cov_storage(m2.m_cov_storage)
        {}

        void operator = (const M2Acc& m2) {m_cov_storage = m2.m_cov_storage;}

template<typename t_sample>
void Push(const t_sample& sample)
{
   for(int i=0; i!=dim; ++i)
       for(int j=0; j<=i; ++j)
           m_cov_storage.At(i,j)(sample.at(i), boost::accumulators::covariate1 = sample.at(j), boost::accumulators::weight = sample.Weight());
}

void PushValue(const t_value value, const t_weight _weight=1.)
{
    Push(value, _weight);
}

//Attention! Dim should be 1
void Push(const t_value value, const t_weight _weight)
{
    m_cov_storage.At(0)(value, boost::accumulators::covariate1 = value, boost::accumulators::weight = _weight);
}

//Attention! Dim should be 2
void Push(const t_value v0, const t_value v1, const t_weight _weight)
{
    m_cov_storage.At(0,0)(v0, boost::accumulators::covariate1 = v0, boost::accumulators::weight = _weight);
    m_cov_storage.At(0,1)(v0, boost::accumulators::covariate1 = v1, boost::accumulators::weight = _weight);
    m_cov_storage.At(1,1)(v1, boost::accumulators::covariate1 = v1, boost::accumulators::weight = _weight);
}

//Attention! Dim should be 3
void Push(const t_value v0, const t_value v1, const t_value v2, const t_weight _weight)
{
    m_cov_storage.At(0,0)(v0, boost::accumulators::covariate1 = v0, boost::accumulators::weight = _weight);
    m_cov_storage.At(0,1)(v0, boost::accumulators::covariate1 = v1, boost::accumulators::weight = _weight);
    m_cov_storage.At(1,1)(v1, boost::accumulators::covariate1 = v1, boost::accumulators::weight = _weight);
    m_cov_storage.At(0,2)(v0, boost::accumulators::covariate1 = v2, boost::accumulators::weight = _weight);
    m_cov_storage.At(1,2)(v1, boost::accumulators::covariate1 = v2, boost::accumulators::weight = _weight);
    m_cov_storage.At(2,2)(v2, boost::accumulators::covariate1 = v2, boost::accumulators::weight = _weight);
}

bool Get(M2<t_value, t_weight, dim>& m2)
{
    if(Count() == 0) return false;

    m2.Count()  = Count();
    m2.Weight() = Weight();
    Mean(m2.Mean());
    Covariance(m2.Covariance());

    return true;
}

t_value Mean(const int dimension)
{
    if(Count() == 0) return 0;

    return boost::accumulators::weighted_mean(m_cov_storage.At(dimension));
}

t_value E(const int dimension){return Mean(dimension);}

void Mean(Algo::Vect< t_value, dim>& mean)
{
    for(int i=0; i!=dim; ++i) mean.at(i) = Mean(i);
}

void E(Algo::Vect< t_value, dim>& mean){Mean(mean);}

t_value Covariance(const int i, const int j)
{
    if(Count() == 0) return 0;

    return boost::accumulators::weighted_covariance(m_cov_storage.At(i,j));
}

t_value Variance(const int i){ return Covariance(i,i); }

void Covariance(CovarianceStorage< t_value, dim >& covariance)
{
    for(int i=0; i!=dim; ++i) for(int j=0; j<=i; ++j) covariance.at(i,j) = Covariance(i,j);
}

unsigned int Count() {return boost::accumulators::count(m_cov_storage.At(0));}

t_weight Weight()const {return boost::accumulators::weighted_sum(m_cov_storage.At(0));}

bool IsVoid(){return Count()==0;}

/// @returns accumulator is not void
bool GetMeanVar(t_value& mean, t_value& variance)
{
    static M2<t_value, t_weight, 1 > moments2;
    const bool isok = Get(moments2);

    mean = moments2.E(0);
    variance = moments2.Variance(0);

    return isok;
}

};


#endif //__M2_ACC_H__
