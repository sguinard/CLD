#ifndef __STRUCT_NEIGHBOR_H__
#define __STRUCT_NEIGHBOR_H__


struct StructNeighbor
{
public :
int k;
float radius;

StructNeighbor():
k(0),
radius(0)
{}

StructNeighbor(
int _k, 
float _radius
):
k(_k),
radius(_radius)
{}

};
#endif //__STRUCT_NEIGHBOR_H__
