#ifndef __STRUCT_NEIGHBORBOUNDS_H__
#define __STRUCT_NEIGHBORBOUNDS_H__


struct StructNeighborBounds
{
public :
int kMin;
int kMax;
float radiusMin;
float radiusMax;

StructNeighborBounds():
kMin(0),
kMax(0),
radiusMin(0),
radiusMax(0)
{}

StructNeighborBounds(
int _kMin, 
int _kMax, 
float _radiusMin, 
float _radiusMax
):
kMin(_kMin),
kMax(_kMax),
radiusMin(_radiusMin),
radiusMax(_radiusMax)
{}

};
#endif //__STRUCT_NEIGHBORBOUNDS_H__
