#ifndef __K_NEAREST_NEIGHBOR_H__
#define __K_NEAREST_NEIGHBOR_H__


struct KNearestNeighbor
{
    KNearestNeighbor(const int k=0, const float distance=0):
            k(k),
            distance(distance),
            distance2(distance*distance)
    {}

    int k;
    float distance;
    float distance2;
};


#endif //__K_NEAREST_NEIGHBOR_H__
