#ifndef DESCRIPTORTOOLS_H
#define DESCRIPTORTOOLS_H

#include <iostream>
#include <map>
#include <utility>

#include "LidarClassifStuff/LidarStructureTensor.h"
#include "LidarClassifStuff/PList/PListStructureTensor.h"
#include "LidarClassifStuff/MinMax.h"
#include "LidarClassifStuff/PCA/Descriptors.h"
#include "LidarClassifStuff/PCA/DescriptorTensor.h"
#include <algorithm>
#include <set>
#include "ANN/MyANN.h"
#include "ANN/ANNLidarAccess.h"

class DimClassic
{

public:

    ListCoord  coord;
    ListStructureTensor     structureTensor;
    ListDimDescriptors      DimDescriptors;

    DimClassic(){}

    void AddToContainer(Lidar::LidarDataContainer& ldc)
    {
        coord.AddToContainer(ldc,Attribute::io_style_t::in_out);
        structureTensor.AddToContainer(ldc,Attribute::io_style_t::in_out);
        DimDescriptors.AddToContainer(ldc,Attribute::io_style_t::in_out);
    }

    static void ChangeSign(const Lg::Point3f& v, float& x, float& y, float& z)
    {
        Lg::Point3f P(x,y,z);
        if( v*P < 0)
        {
            x *= -1;
            y *= -1;
            z *= -1;
        }
    }

    void ChangeSign(Lidar::LidarDataContainer& ldc)
    {
        Lg::Point3f origin = coord.Point(ldc.begin());
        origin += Lg::Point3f(1000, 1000, 10000);

        Lidar::LidarDataContainer::iterator first=ldc.begin();
        Lidar::LidarDataContainer::iterator last=ldc.end();
        for(Lidar::LidarDataContainer::iterator it = first; it!=last; ++it)
        {
            Lg::Point3f v = coord.Point(it)-origin;
            ChangeSign(v,
                       structureTensor.eigenVector3x.Value(it),
                       structureTensor.eigenVector3y.Value(it),
                       structureTensor.eigenVector3z.Value(it));

            ChangeSign(v,
                       structureTensor.eigenVector2x.Value(it),
                       structureTensor.eigenVector2y.Value(it),
                       structureTensor.eigenVector2z.Value(it));

            ChangeSign(v,
                       structureTensor.eigenVector1x.Value(it),
                       structureTensor.eigenVector1y.Value(it),
                       structureTensor.eigenVector1z.Value(it));
        }
    }

    /*void Fill(Lidar::LidarDataContainer::iterator it,
              StructureTensorDescriptor structure_tensor,
              float k_opt,
              float radius,
              float z)
    {
        StructStructureTensor sst(structure_tensor.GetStruct());
        structureTensor.Set(it, sst);
        DimDescriptors.k.Value(it) = k_opt;
        DimDescriptors.radius.Value(it) = radius;
        DimDescriptors.D1.Value(it) = 0;
        DimDescriptors.D2.Value(it) = 0;
        DimDescriptors.D3.Value(it) = 0;

    }*/

    /*void FillNN(Lidar::LidarDataContainer::iterator it, StructureTensorDescriptor structure_tensor, float k_opt, float radius)
    {
        DimDescriptors.D1.Value(it) = structure_tensor.GetDim().linearity();
        DimDescriptors.D2.Value(it) = structure_tensor.GetDim().planarity();
        DimDescriptors.D3.Value(it) = structure_tensor.GetDim().scattering();
    }*/

    void RunNN(const Lidar::LidarDataContainer::iterator first_in,
             const Lidar::LidarDataContainer::iterator first_fill,
             const Lidar::LidarDataContainer::iterator last_fill,
             const Lidar::LidarDataContainer::iterator last_in,
             const int kmin, const int kmax, const int kstep)
    {
        //Tests sur NN
        std::cout << "\t\tCreating ANN" << std::endl;
        ANNLidarAccess lidar_access(&coord);
        MyANN myAnn(3, last_in-first_in);
        myAnn.BuildTree(first_in, last_in, lidar_access);

        NearestNeighbors nn(kmax);

        cout << "\t\tAccumulating tensors" << endl;
        float zmax=-1e6;
        float zmin=1e6;
        for(Lidar::LidarDataContainer::iterator it = first_fill; it!=last_fill; ++it)
        {
            if (coord.Point(it).z()>zmax) zmax=coord.Point(it).z();
            if (coord.Point(it).z()<zmin) zmin=coord.Point(it).z();

            Lg::Point3f point(coord.Point(it));

            myAnn.QueryPoint()[0]=point.X();
            myAnn.QueryPoint()[1]=point.Y();
            myAnn.QueryPoint()[2]=point.Z();

            myAnn.Query(nn, 0.00001);

            StructureTensorDescriptor structure_tensor;

            Algo::Min<float, std::pair<int,StructureTensorDescriptor> > entropy_min;

            float z = 0;
            float d=0;

            // loop on k-nearest neighboors of it
            for (int ki=0; ki<kmax; ++ki)
            {
                const Lg::Point3f neig(coord.Point(first_in + nn.idx[ki]));

                structure_tensor(LGCOORD3(neig));

                if(ki >= kmin && ki%kstep == 0)
                {
                    structure_tensor.FindEigen();
                    entropy_min(structure_tensor.GetDim().EigenEntropy(true), std::pair<int,StructureTensorDescriptor>(ki,structure_tensor));
                }

            }
            const int k_opt = entropy_min.Id().first;
            for (int i=0; i!=k_opt; ++i)
            {
                Lg::Point3f voisin(coord.Point(first_in + nn.idx[i]));

                myAnn.QueryPoint()[0]=voisin.X();
                myAnn.QueryPoint()[1]=voisin.Y();
                myAnn.QueryPoint()[2]=voisin.Z();
            }

            // Get structure
            StructStructureTensor sst(structure_tensor.GetStruct());

            // Define verticality vector
            float vert_x = 0;
            float vert_y = 0;
            float vert_z = 0;

            // Find eigenvalues and eigenvectors
            float lambda1 = sst.lambda1;
            float lambda2 = sst.lambda2;
            float lambda3 = sst.lambda3;
            float x1 = abs(sst.eigenVector1x);
            float y1 = abs(sst.eigenVector1y);
            float z1 = abs(sst.eigenVector1z);
            float x2 = abs(sst.eigenVector2x);
            float y2 = abs(sst.eigenVector2y);
            float z2 = abs(sst.eigenVector2z);
            float x3 = abs(sst.eigenVector3x);
            float y3 = abs(sst.eigenVector3y);
            float z3 = abs(sst.eigenVector3z);

            // Verticality vector = sum( eigenvalues * eigenvectors )
            vert_x += (lambda1*x1 + lambda2*x2 + lambda3*x3);
            vert_y += (lambda1*y1 + lambda2*y2 + lambda3*y3);
            vert_z += (lambda1*z1 + lambda2*z2 + lambda3*z3);

            // Normalize verticality vector
            float norm = sqrt( vert_x*vert_x + vert_y*vert_y + vert_z*vert_z );
            vert_z /= norm;

            // Assign vertical component of verticality vector
            DimDescriptors.verticality.Value(it) = vert_z;

            // Assign dimensionality descriptors
            DimDescriptors.linearity.Value(it)  = structure_tensor.GetDim().linearity();
            DimDescriptors.planarity.Value(it)  = structure_tensor.GetDim().planarity();
            DimDescriptors.scattering.Value(it) = structure_tensor.GetDim().scattering();
        }

    }

    void RunNN(const Lidar::LidarDataContainer::iterator first,
             const Lidar::LidarDataContainer::iterator last,
             const int kmin, const int kmax, const int kstep)
    {
        RunNN(first, first, last, last, kmin, kmax, kstep);
    }

};


#endif // DESCRIPTORSTOOLS

