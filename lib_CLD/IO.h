#ifndef IO
#define IO

#include <iostream>
#include "DescriptorsTools.h"


void ComputeLocalDescriptors(std::string file, int kmin, int kmax, int kstep)
{

    std::cout 	<< "\nFile is : \n\t"	<< file
                << "\n\n\tLoading"		<< std::endl;

    Lidar::LidarDataContainer ldc(file);		// load
    DimClassic dim_classic;
    dim_classic.AddToContainer(ldc);

    std::cout	<< "\tComputing Local Descriptors" << std::endl;
    dim_classic.RunNN(ldc.begin(),ldc.end(),kmin,kmax,kstep);		// compute local descriptors

    std::cout   << "\tSaving " << file << "_descriptors.ply" << std::endl;
    const char* del_attr[] = {"eigenVector1x","eigenVector1y","eigenVector1z",
                        "eigenVector2x","eigenVector2y","eigenVector2z",
                        "eigenVector3x","eigenVector3y","eigenVector3z",
                        "lambda1","lambda2","lambda3",
                        "sigma1","sigma2","sigma3",
                        "D1","D2","D3",
                        "gx","gy","gz",
                        "Dmax","entropy","similarity","radius"};
    std::vector<std::string> attributes_to_delete(del_attr,del_attr+25);
    ldc.delAttributeList(attributes_to_delete);				// remove every attribute not needed anymore
    ldc.save(file+"_descriptors.ply");

}

#endif // IO
