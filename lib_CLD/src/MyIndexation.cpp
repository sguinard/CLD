
#include "../ANN/MyIndexation.h"

void MyIndexation( 	const std::string& nomfic_in,
                        const std::string& nomfic_neigh,
                        const int k,
                        const int dim,
                        const double eps)
{
    /////////////////////////////////////////////////////////////////
    time_t start;
    time (&start);
    /////////////////////////////////////////////////////////////////

    Lidar::LidarDataContainer ldc_out;
    Lidar::LidarCenteringTransfo transfo;
    ListCoord coord_write;

    std::cout << __FILE__ << " : " << __LINE__ << std::endl;
    std::cout << "Nombre de voisins : "  << k << std::endl;

    {
        Lidar::LidarDataContainer ldc_in;
        Lf::LoadData(ldc_in, transfo, nomfic_in);
        ListCoord coord_read;
        coord_read.AddToContainer(ldc_in, Attribute::in);
        coord_write.AddToContainer(ldc_out, Attribute::out);
        coord_write.Copy(coord_read);
    }

    std::cout << __FILE__ << " : " << __LINE__ << std::endl;

    ListKNearestNeighbors lknn(k);
    std::cout << __FILE__ << " : " << __LINE__ << std::endl;
    lknn.AddToContainer(ldc_out, Attribute::out);

    std::cout << __FILE__ << " : " << __LINE__ << std::endl;

    if(dim==2)
    {
        ANNLidarAccessPoint2D lidar_access(&coord_write);
        NeighborsStorage(lknn, lidar_access, 2, eps);
    }
    else //dim = 3
    {
        ANNLidarAccess lidar_access(&coord_write);
        NeighborsStorage(lknn, lidar_access, 3, eps);
    }

    Lf::Save(ldc_out, transfo, nomfic_neigh);

    /////////////////////////////////////////////////////////////////
    time_t end;
    time (&end);
    double execution_time = difftime (end,start);
    printf ("It took %.2lf seconds to do ANNIndexation.\n", execution_time );
    /////////////////////////////////////////////////////////////////
}
