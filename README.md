Local Descriptors Computation library by Stephane Guinard for IGN-France

Required dependencies: Ann, Boost (system filesystem thread serialization), LidarFormat

Test:

Assuming your build dir is also in the root, go to it and:

./CLD_exe ../../data/bildstein_station1_test.ply

The output point cloud must contains 3kk points with x, y and z coordinates and 
4 local descriptors: 
	linearity
	planarity
	scattering
	verticality

For more information, please refer to:
Guinard, S. and Landrieu, L., 2017
Weakly supervised segmentation-aided classification of urban scenes from 3D LiDAR point clouds
https://www.int-arch-photogramm-remote-sens-spatial-inf-sci.net/XLII-1-W1/151/2017/isprs-archives-XLII-1-W1-151-2017.pdf



